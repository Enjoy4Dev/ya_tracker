
// SERVER INFO START
const DOMAIN = 'https://test-oms.dr-telemed.ru'
const METHOD = '/ya-tracker-data'
const DATA_PARAM_KEY = '?data='
const EPIC_COLORS = ['#449837', '#5a5096', '#ff5c00', '#ffc800', '#cc2944', '#2d9cdb']
// SERVER INFO END	

var Toast = function (element, config) {
  var _this = this,
    _element = element,
    _config = {
      autohide: true,
      delay: 5000,
    };
  for (var prop in config) {
    _config[prop] = config[prop];
  }
  Object.defineProperty(this, "element", {
    get: function () {
      return _element;
    },
  });
  Object.defineProperty(this, "config", {
    get: function () {
      return _config;
    },
  });
  _element.addEventListener("click", function (e) {
    if (e.target.classList.contains("toast__close")) {
      _this.hide();
    }
  });
};

Toast.prototype = {
  show: function () {
    var _this = this;
    this.element.classList.add("toast_show");
    if (this.config.autohide) {
      setTimeout(function () {
        _this.hide();
      }, this.config.delay);
    }
  },
  hide: function () {
    var event = new CustomEvent("hidden.toast", {
      detail: { toast: this.element },
    });
    this.element.classList.remove("toast_show");
    document.dispatchEvent(event);
  },
};

Toast.create = function (props) {
  const { text, color, autohide, delay } = props;
  var toast = document.createElement("div"),
    loader = document.createElement("div"),
    toastClose = document.createElement("button");
  toast.classList.add("toast");
  toast.style.backgroundColor =
    "rgba(" +
    parseInt(color.substr(1, 2), 16) +
    "," +
    parseInt(color.substr(3, 2), 16) +
    "," +
    parseInt(color.substr(5, 2), 16) +
    ",0.73)";
  toast.textContent = text;
  toastClose.classList.add("toast__close");
  toastClose.setAttribute("type", "button");
  toastClose.textContent = "×";
  loader.classList.add("toast__loader");
  loader.style.animationDuration = delay + "ms";
  toast.appendChild(toastClose);
  if (autohide) {
    toast.appendChild(loader);
  }
  return toast;
};

Toast.add = function (params) {
  var config = {
    header: "Название заголовка",
    text: "Текст сообщения...",
    color: "#f73454",
    autohide: true,
    delay: 10000,
  };
  if (params !== undefined) {
    for (var item in params) {
      config[item] = params[item];
    }
  }
  if (!document.querySelector(".toasts")) {
    var container = document.createElement("div");
    container.classList.add("toasts");
    // container.style.cssText =
    //   "position: fixed; top: 50px; right: 50px; z-index: 1140;";
    document.body.appendChild(container);
  }
  document
    .querySelector(".toasts")
    .appendChild(
      Toast.create(config)
    );
  var toasts = document.querySelectorAll(".toast");
  var toast = new Toast(toasts[toasts.length - 1], {
    autohide: config.autohide,
    delay: config.delay,
  });
  toast.show();
  return toast;
};

document.addEventListener("hidden.toast", function (e) {
  var element = e.detail.toast;
  if (element) {
    element.parentNode && element.parentNode.removeChild(element);
  }
});


const statuses = ['blocker', 'crit', 'normal', 'minor', 'trivial']
const getRandom = (arr = EPIC_COLORS) => arr[Math.round(Math.random() * (arr.length - 1))]
// const fishText =
// 	'Разнообразный и богатый опыт реализация намеченных плановых заданий в значительной степени обуславливает создание новых предложений. Повседневная практика показывает, что постоянное информационно-пропагандистское обеспечение нашей деятельности способствует подготовки и реализации соответствующий условий активизации. С другой стороны постоянный количественный рост и сфера нашей активности играет важную роль в формировании дальнейших направлений развития. Товарищи! постоянное информационно-пропагандистское обеспечение нашей деятельности требуют от нас анализа позиций, занимаемых участниками в отношении поставленных задач. Товарищи! начало повседневной работы по формированию позиции способствует подготовки и реализации позиций, занимаемых участниками в отношении поставленных задач. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности позволяет оценить значение дальнейших направлений развития.'

const itemTemplate = {
  epic: {
    title: 'Редизайн МП',
    task: 'IT-152',
    sprint: '26 спринт',
    color: getRandom()
  },
  title: 'Back_Доработка объекта “Продукт” для скрытия и продуктов для части партнеров',
  link: 'https://tracker.yandex.ru/IT-39',
  task: 'IT-162',
  customer: 'Котляр И.О.',
  executor: 'Лузанов М.О.',
  time: '1 ч',
  status: getRandom(statuses),
  hidden: false,
  id: new Date().getTime() * parseInt('IT-162'.replace(/\D/g, "")) * (1) + '___' + 'IT-162'
}

Vue.component('status', {
  props: ['priority', 'status'],
  template: `
    <div v-if="priority === 'blocker'" class="card__status" :data-status="priority">
      <span>БЛОКЕР</span>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M19.3 11.3C19.6866 11.6866 19.6866 12.3134 19.3 12.7C18.9134 13.0866 18.2866 13.0866 17.9 12.7L13 7.8L13 20L11 20L11 7.8L6.1 12.7C5.7134 13.0866 5.0866 13.0866 4.7 12.7C4.3134 12.3134 4.3134 11.6866 4.7 11.3L12 4L19.3 11.3Z" fill="white"/>
        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="4" y="4" width="16" height="16">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M19.3 11.3C19.6866 11.6866 19.6866 12.3134 19.3 12.7C18.9134 13.0866 18.2866 13.0866 17.9 12.7L13 7.8L13 20L11 20L11 7.8L6.1 12.7C5.7134 13.0866 5.0866 13.0866 4.7 12.7C4.3134 12.3134 4.3134 11.6866 4.7 11.3L12 4L19.3 11.3Z" fill="white"/>
        </mask>
        <g mask="url(#mask0)">
        </g>
      </svg>
    </div>
    <div v-else-if="priority === 'critical'" class="card__status" :data-status="priority">
      <span>КРИТИЧНЫЙ</span>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M19.3 11.3C19.6866 11.6866 19.6866 12.3134 19.3 12.7C18.9134 13.0866 18.2866 13.0866 17.9 12.7L13 7.8L13 20L11 20L11 7.8L6.1 12.7C5.7134 13.0866 5.0866 13.0866 4.7 12.7C4.3134 12.3134 4.3134 11.6866 4.7 11.3L12 4L19.3 11.3Z" fill="white"/>
        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="4" y="4" width="16" height="16">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M19.3 11.3C19.6866 11.6866 19.6866 12.3134 19.3 12.7C18.9134 13.0866 18.2866 13.0866 17.9 12.7L13 7.8L13 20L11 20L11 7.8L6.1 12.7C5.7134 13.0866 5.0866 13.0866 4.7 12.7C4.3134 12.3134 4.3134 11.6866 4.7 11.3L12 4L19.3 11.3Z" fill="white"/>
        </mask>
        <g mask="url(#mask0)">
        </g>
      </svg>
    </div>
    <div v-else-if="priority === 'normal'" class="card__status" :data-status="priority">
      <span>СРЕДНИЙ</span>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="12" cy="12" r="4" fill="white"/>
      </svg>
    </div>
    <div v-else-if="priority === 'minor'" class="card__status" :data-status="priority">
      <span>НИЗКИЙ</span>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M4.7 12.7C4.3134 12.3134 4.3134 11.6866 4.7 11.3C5.0866 10.9134 5.7134 10.9134 6.1 11.3L11 16.2L11 4L13 4L13 16.2L17.9 11.3C18.2866 10.9134 18.9134 10.9134 19.3 11.3C19.6866 11.6866 19.6866 12.3134 19.3 12.7L12 20L4.7 12.7Z" fill="white"/>
        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="4" y="4" width="16" height="16">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M4.7 12.7C4.3134 12.3134 4.3134 11.6866 4.7 11.3C5.0866 10.9134 5.7134 10.9134 6.1 11.3L11 16.2L11 4L13 4L13 16.2L17.9 11.3C18.2866 10.9134 18.9134 10.9134 19.3 11.3C19.6866 11.6866 19.6866 12.3134 19.3 12.7L12 20L4.7 12.7Z" fill="white"/>
        </mask>
        <g mask="url(#mask0)">
        </g>
      </svg>
    </div>
    <div v-else-if="priority === 'trivial'" class="card__status" :data-status="priority">
      <span>НЕЗНАЧИТЕЛЬНЫЙ</span>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clip-path="url(#clip0)">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M4.7 12.7C4.3134 12.3134 4.3134 11.6866 4.7 11.3C5.0866 10.9134 5.7134 10.9134 6.1 11.3L11 16.2L11 4L13 4L13 16.2L17.9 11.3C18.2866 10.9134 18.9134 10.9134 19.3 11.3C19.6866 11.6866 19.6866 12.3134 19.3 12.7L12 20L4.7 12.7Z" fill="black"/>
        <path fill-rule="evenodd" clip-rule="evenodd" d="M4.7 4.7C4.3134 4.3134 4.3134 3.6866 4.7 3.3C5.0866 2.9134 5.7134 2.9134 6.1 3.3L11 8.2L11 -4L13 -4L13 8.2L17.9 3.3C18.2866 2.9134 18.9134 2.9134 19.3 3.3C19.6866 3.6866 19.6866 4.3134 19.3 4.7L12 12L4.7 4.7Z" fill="black"/>
        <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="4" y="4" width="16" height="16">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M4.7 12.7C4.3134 12.3134 4.3134 11.6866 4.7 11.3C5.0866 10.9134 5.7134 10.9134 6.1 11.3L11 16.2L11 4L13 4L13 16.2L17.9 11.3C18.2866 10.9134 18.9134 10.9134 19.3 11.3C19.6866 11.6866 19.6866 12.3134 19.3 12.7L12 20L4.7 12.7Z" fill="white"/>
        </mask>
        <g mask="url(#mask0)">
        </g>
        </g>
        <defs>
        <clipPath id="clip0">
        <rect width="24" height="24" fill="white" transform="translate(0 24) rotate(-90)"/>
        </clipPath>
        </defs>
      </svg>
    </div>
    <div v-else class="card__status">
      <span>Без приоритета</span>
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="12" cy="12" r="4" fill="white"/>
      </svg>
    </div>
    `
})

Vue.component('card-item', {
  props: ['card', 'removekey', 'removeitem', 'onlyshow'],
  template: `
			<div class="card">
				<div v-if="onlyshow" class="card__close" @click="removeitem(removekey)"></div>
				<div class="card_header" :style="'background-color:' + card.epic.color || '#b4b4b4'">
				<p class="card_header__title" v-bind:style="{fontSize: titleSize + 'px', lineHeight: titleSize * 1.2 + 'px'}">
					<span v-if="card.epic.title">{{ card.epic.title }}</span>
				</p>
				<div class="card_header__info">
					<p>{{ card.epic.task }}</p>
					<p>
						<span v-for="(sprint, index) in card.sprint">
							<span v-if="index > 0">, </span>
							{{ sprint }}
						</span>
					</p>
				</div>
				</div>
				<div class="card_body">
				<div class="card_body__info">
					<p>
					{{ card.title }}
					</p>
					<a :href="card.link" target="_blank" class="qrcode" ref="qrref"></a>
				</div>
				<p class="card_body__task">
					<span>{{ card.task }}</span>
					<span>{{ card.customer }}</span>
				</p>
				<p class="card_body__executor">
					<span>{{ card.executor }}</span>
					<span>{{ card.time }}</span>
				</p>
				</div>
				<status v-bind:priority="card.priority" v-bind:status="card.status"></status>
			</div>
		`,
  mounted: function () {
    this.$nextTick(function () {
      const link = this.card.link
      var qrElement = this.$refs.qrref
      const qrCode = new QRCodeStyling({
        width: 108,
        height: 108,
        data: link,
        image:
          'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAnnSURBVHgB7V1Ji1RJEE6bdl8R9xV3xV1RUfSgIl69+Q/8AeLBm4iIJ0FBxZuIR0FE9KIXPakHcQG9iAvuC4K7IujMxGOiJipeREZEVlXbDv3Bo19mZUZGRsaW+V5V9/vrH6Q+FKMr9aEldKcexpcvX9KDBw/Sw4cP06NHj9KbN2/S27dvq3q4+vXr17gAtCzVWWWtTVdXV1M93s+bNy/t3LkzedEjAvz8+XO6ePFiunr1aiW4r1+/ipPjgDruYXgd9sM6b592ea6OCvDWrVvp1KlT6c6dOzWBUeCEPJOXgH1z9Cw6fCG86IgAQXAnT55Mt2/fVrULAZ/9+vWrJtycxkhal+tDBYrQFuy3CvDVq1fpwIEDNcFZ5tMus9Pa0AWy+vw2AZ4+fTqdOHGiCgQc3KRKhajRzdVLi5gz+ShaFiBo3f79+9PNmzdrkY5PDjQBoh9lNrLimkbTOq+mSnSAvx7VwPv376fdu3dXQkQmNAbwM8ucEF7/5oVGW1qMCIoFeOHChXTo0KEqRUGtk0yQM0zLtF2JQKxAI9G0TLVHNPD8+fNp37597twth5wfA1iayNtIQtQisuSPv337lh4/ftxEn7umQYMGpTFjxlT3YQHeuHEj7d27t4k4nxiHJ4hIkLRFippSQNDKCO6Psc3Pnz+brAppwIW7F4qQAF+8eJF27dplCsGby+W0SasrGYvXWWNhPV8cyX+7BQjC27FjR/r48WORz5IYKqXj9belqZLls2nZLcDjx4+n58+fqybliaDRSGhNhMO7QLmFk1IZPn5YgOfOnasuabK5tCTH8MSJE9OyZctqfob2wb/gk65du1ajySfjdR0Wzx5/jjAFCKZ77NgxM7+LYunSpVUO6cHr16+rk5zSsThy/pf6QArUbhA+hSlAEB413ZIAQet5Xy+sVIWPUxqgNAFKrgWQFSBo39mzZ91MenI0uDB9iCC6YF5tlXJEj69FZAV49OjRbFTTGLAY5mbggYcub6ctIuUTy5GxKFQBgtmeOXPGTFK5Q26ndiCk6KeZoEUbedYONTwHCi4NPHLkSK2xxpBHM5GW10wkpiO+zOJZisRtNeHr16/X1B0H4UQ0U5HgSXtyTFMfSicspUDSAtI2kjVhu9wcTAGC8J49e9YUeaMaozGJDJQGEWqCuBiRgMFpSUKFy7vQogDB90WjLq3TzLcVeHyv1E5KU2gWwNtI/jbHi6qBGuMeIfLJcRPjdCOILoxmqpoQI9oHqAkQou/Tp09VAl6faJWj0AQVsQxazgmRjinNL6uB9+7dCwtIY5L24+VWfGBU66R6LRLzNAbb4Fkg9981Ad69e7fRMMLk/Pnz04gRIxqfcf/E6+bOnZsigIMHTlv6i/fwygjsoTV+pbpckKHRPqSBGnE+wOHDh9PkyZNTJzBhwoSKfgQHDx5Mly5dCguNaxh+TrU1K8APHz40SdqT2ZcGhE6CagyWEVaOF5lPTYCQ/+X8RG8VmASqVVbgsnJUnlUgagJ88uSJGZ3aEVU7DSoIafJaGpTTQGkhxDzwT9Y6RE6TcuacEyDdqSC6pYE1rcPPebm3aqB2b/Eb8fndWmctiGhJaW+ERwN5nSeIuDXw/xCJAZYSYB328cwZ4fKBHH+CT7Q0Sdu6DRgwwOxb5AM9DPRGRCIxlIcPH17de4/Ksj4wV8Y6LEPWP3LkSE5O1eQpU6akVatWJQ8+ffqUrly5ItLi2zm8f/nyZZEygACp8CRrpO/IiCZs+QsJ+LYWDka/RoB/6bVt2za3AOHBOrzQJNHhdVrZ4wOhDt+6gnvNlXV3/ye2mgCls7xcYm1pptYmgpxjlwIe7cfbaWUECFDbAmK/gQMHNsouDfScnUmw9pwRRHZGVr2GcePGNYIIp0EPFbIaKK1iZDtnRT5sUypAaexcH+4bKS1OZ9KkSepCUL8IL1gi1CDiZYD3kQZvR7T2aJ0lII0/7DNz5kwXr24BeiAJ2TInyTFHxuM0c4KS2kjt4TBY00Dsg+NjqgPo9jCYgzeISElrBLmJWT6amqDGExwGezR39OjRTeVui2GPgDSmPf28sIJIye6J1q1evbqhqTlaVPsAbhNup4BKBRmJxFI6o/VbuXJlGjZsWDb3Q0CkpnDvRDQGOCzzxRUu9bUl/Fn+D7SP05LQv39/nwlHhFQq1Cg8zt0Lys+SJUsar4lY7y1y7QOYO5GIQ9b+Yhs60ciErbyR7pY8ERnvAcuXL2/yfTkhjh8/vlanaqCXEQr+vV8p8uLn8HUJeHFc00ZaD29LREzX6//AdNH3YRvtFGbIkCHV41UO13FWadTl7eg9/AQAnOBQgdOL15WAaibnB/I+OMyQ+MW+FNz3Idx5oEeInrp2pUW5PpILoYCvWGzdutV0C3Th4M0LCaGdSCTySX09ZhehXZoTrl+/vsl0NdrIz/Tp0ysTluB6qETrJeY92qH1a0daJAUsrQ/4vbFjx7oWHV2Apn0ANYhYX4zWPrcicST6cpq5smc7t3HjxrRw4cKsYvBxFyxYkIYOHaq2CT0X9vgtHomtly09pqyNlxub0wLhgTBoGsV54QCzBfPNoaWdSImgS4MIR8RngtlS4XktAbQ1p32A8HFWqz7IohMZW2uD84CN//bt26vzO49PppgxY0Z1WXCdxniY9pyGULoS7UiqopUR4Pg3bNhQO6LX+KZ0IEIvXrw4eSBqoOSn8DPKtCcSt6ppUSFCggxpCgjwx48f7qCFgoQHRps3bzZNF2EGkdy9JxK34y0HHoS0NrCzgMnDc+Tv379nx+W0kS5oHmigF2YQifxsEtZZe+ISH4m88e+4gZ+DIIG5Grxh6xGcBHgPG4JNBG0LIlIb+pfT9vi73NgwWXiGAV/choMJLYXxzmvFihWNF9kjcD0Xluo8fi36vbgcXXjgPWvWrOrZBWgbCA3w/v377BiotTnAkVaJ8Coe/2Izh5+tQ+abGv5bhh+muXz5cpOJ4r32Ood2b9UBa7Nnz678GW8j9dGuXNt169alRYsWpVLUBGjh3bt3ac+ePe4JYDv+k5taW287L02JLtxDtIUTGXADraDop5+k3MnyYZ51QhoWLfycp1vefA+OszZt2lR7wlaCIgHS/SSi9PBBok01RwOlySOz1h6S6jVr1lSBp10o/vU2PLGxHsRwbfJqlxS8tLaelyEh+GzZsqXxdbR2oVgDqRlFEPmhHpr3edpLgoQXOdeuXVv97QRaMmFLmyg8GpUbK0eXWwKUp02bVkXYqVOnpk6iJQHivfbgRkKJiXpogtAGDx5cbefmzJkjPsPtBFoWoDTJnJl6oixvT38fgQKOqeBRIxx6wgVa19MoEiB+1zeXo9HPPfdaGQCahdeoUaOqCwQHf383wol0H5rR998cWsTfPocK+DDk6z4AAAAASUVORK5CYII=',
        dotsOptions: {
          color: '#000',
          type: 'square'
        },
        backgroundOptions: {
          color: '#FFFFFF'
        }
      })
      qrCode.append(qrElement)
    })
  },
  computed: {
    titleSize: function () {
      const length = this.card.epic.title?.length || 0
      const size = length < 30
        ? 30
        : 30 * ((100 - (length - 30)) / 100)
      return size < 14 ? 14 : size
    }
  }
})

var app = new Vue({
  el: '#app',
  data: {
    // cards: [itemTemplate],
    cards: [],
    // inputValue: 'sprint-11;IT-198;IT-128;IT-313;sprint-21',
    inputValue: '',
    loading: false,
    timeout: null,
    palletteCurrentEpic: null,
    defaultColors: [...EPIC_COLORS],
    tempColor: '#b4b4b4'
  },
  methods: {
    startLoading() {
      this.loading = true
      this.timeout = setTimeout(() => {
        this.loading = false
      }, 10000)
    },
    stopLoading() {
      this.loading = false
      this.timeout && clearTimeout(this.timeout)
      this.timeout = null
    },
    async inputHandler() {
      if (this.loading) {
        Toast.add({
          text: `Данные загружаются, подождите`,
          color: "#f73454",
          delay: 2000,
        })
        return
      }
      const outgoingIds = this.inputValue.split(';')
      const tasks = outgoingIds.filter(item => !item.includes('sprint'))
      const sprints = outgoingIds.filter(item => item.includes('sprint'))
      const data = []
      const tasksRequest = tasks.length ? await this.getTasks(tasks) : []
      data.push(...tasksRequest)
      for (const sprint of sprints) {
        const sprintRequest = await this.getSprint(sprint)
        data.push(...sprintRequest)
      }
      data.forEach(item => this.createCard(item))
      this.stopLoading()
    },
    async get(query) {
      const data = []
      let page = 1
      const fetchNow = async () => {
        this.timeout && clearTimeout(this.timeout)
        this.startLoading()
        try {
          const response = await fetch(`${DOMAIN}${METHOD}${DATA_PARAM_KEY}${query.replace(/\s/g, '')}${`&page=${page}`}`);
          if (response.status === 200) {
            const contentType = response.headers.get("content-type");
            if (contentType && contentType.indexOf("application/json") !== -1) {
              const responseJSON = (await response.json());
              const dataNow = responseJSON?.data || [];
              const request = dataNow.map((item, i) => ({
                ...item,
                id: new Date().getTime() * parseInt(item.task.replace(/\D/g, "")) * (i + 1) + '___' + item.task
              }))
              data.push(...request)
              if (responseJSON?.pageCount && (page < responseJSON.pageCount)) {
                page++
                return await fetchNow()
              } else {
                console.log('NOW ', data)
                return data
              }
            } else {
              Toast.add({ text: `Вы не авторизованы` });
              return [];
            }
          } else {
            Toast.add({ text: `С запросом ${query} что то пошло не так, подробности в консоли` });
            console.error(response);
            return [];
          }
        } catch (e) {
          return [];
        }
      }
      return await fetchNow();
    },
    async getTasks(arr) {
      const query = arr.join(';')
      if (typeof query !== 'string' || query.length === 0) {
        Toast.add({ text: `Нельзя отправлять пустую строку` })
        return []
      }
      const data = await this.get(query)

      const incomingIds = data.map(item => item.task)
      const errorKeys = arr.filter(item => !incomingIds.some(key => key === item))
      errorKeys.forEach(item =>
        Toast.add({ text: `Задача ${item} \  не найдена` })
      )
      return data
    },
    async getSprint(query) {
      const data = await this.get(query)
      if (data.length === 0) {
        Toast.add({ text: `Спринт ${query} \  не найден` })
      }
      return data
    },
    clearAll() {
      this.inputValue = ''
    },
    createCard(item) {
      const isExist = this.cards.some(card => item.task === card.task)
      if (!isExist) {
        this.cards = [
          ...this.cards,
          {
            ...item,
            epic: item.epic
              ? {
                ...item.epic,
                title: item.epic.title?.slice(0, 150) || '',
                // TODO REAL COLOR
                color: item.epic.color || '#b4b4b4'
              }
              : false
          }
        ]
      } else {
        Toast.add({
          text: `Задача ${item.task} \  уже в списке`,
          color: "#f7bd02",
        })
      }
    },
    removeItem(key) {
      this.cards = this.cards.filter(item => item.id !== key)
    },
    epicPalleteHandler(epic) {
      this.palletteCurrentEpic = epic
      if (epic) {
        this.changeTempColor(epic.color)
      }
    },
    changeTempColor(color) {
      this.tempColor = color
    },
    colorPickerHandler(color) {
    },
    changeEpicColor(id) {
      this.cards = this.cards.map(card => {
        const item = { ...card }
        if (card.epic?.task === id) {
          item.epic = { ...item.epic, color: this.tempColor }
        }
        return item
      })
      this.palletteCurrentEpic = null
    }
  },
  computed: {
    pages: function () {
      const pages = []
      for (var i = 0; i < this.cards.length; i = i + 6) {
        pages.push([...this.cards].slice(i, i + 6))
      }
      return pages
    },
    epics: function () {
      return this.cards
        .map(card => card.epic)
        .filter((epic, index, self) =>
          index === self.findIndex(e => (
            epic && epic.title === e.title
          ))
        )
    },
    colors: function () {
      return this.defaultColors.filter(color => !this.epics.some(item => item.color === color))
    }
  }
})